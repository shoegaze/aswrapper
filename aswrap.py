#!/usr/bin/env python3

# Use at your own risk

import utils
from os import path, getenv, makedirs, chdir
from shlex import quote

if not path.exists('%s/.cache/aswrapper' % getenv('HOME')):
    makedirs('%s/.cache/aswrapper' % getenv('HOME'))

try:
    from setproctitle import setproctitle
    setproctitle('aswrapper') # Set process title to 'aswrapper'
except ImportError:
    pass
var = utils.variables()
io = utils.interactiveness()
pkgutils = utils.build_functions()
colors = utils.colors()

if var.arguments.update or var.arguments.updateall: # TODO: add checks
    from pkg_resources import parse_version
    var.packagelist = set()
    updatelist = pkgutils.get_update_packages()
    for data in updatelist:
        if data[1] in [ 'git', 'svn', 'hg', 'bzr' ]:
            if var.arguments.updateall:
                var.packagelist.add(data[0])
        else:
            if parse_version(pkgutils.get_aur_pkgversion(data[0])[0]) > parse_version(data[1]): # Higher ver num
                var.packagelist.add(data[0])
            elif parse_version(pkgutils.get_aur_pkgversion(data[0])[0]) > parse_version(data[1]) and pkgutils.get_aur_pkgversion(data[0])[1] > data[2]: # Higher pkgrel num
                var.packagelist.add(data[1])
    if len(var.packagelist) > 0:
        io.warn('Following AUR packages are going to be updated:')
        for package in var.packagelist:
            io.warn(package, False, colors.header)
        if not io.prompt('Do you want to continue?'):
            quit(0)
    else:
        io.warn('No AUR updates available')

try: # Main code block
    if var.arguments.purgecache:
        if io.prompt('Remove .cache/aswrapper contents?'):
            io.warn('Purging cache...')
            pkgutils.cleanup(var.cachedir)
        quit()

    for package in var.packagelist:
        __pkginfo = pkgutils.get_pkginfo(package)
        pkgbase, isRepo, dependencies = __pkginfo[0], __pkginfo[1], pkgutils.get_deps(package)
        if not pkgbase:
            io.fail('Package not found: %s' % package, True)
            continue
        if pkgbase in var.used_pkgbases:
            io.warn('Skipping %s as it has already been built')
            continue
        
        if path.exists('%s/%s' % (var.builddir, pkgbase)):
            if io.prompt('Build directory for %s is not empty, remove it?' % pkgbase) or var.arguments.noconfirm:
                if not pkgutils.cleanup('%s/%s' % (var.builddir, pkgbase)):
                    io.fail('Removing failed! Skipping package %s' % pkgbase, True)
                    continue
            else:
                io.warn('Skipping package %s' % pkgbase)
                continue

        if not path.exists(var.builddir):
            makedirs(quote(var.builddir))
        chdir(quote(var.builddir))
        pkgutils.fetch_package(isRepo, pkgbase)
        try:
            chdir(quote('%s/%s' % (var.builddir, pkgbase)))
        except FileNotFoundError:
            io.fail('Directory \"%s\" does not exist!' % pkgbase, True)
            continue
        
        if not var.arguments.noconfirm:
            if io.prompt('Edit PKGBUILD?'):
                pkgutils.edit_package()
        while True: # TODO: Copy package files to some other dir
            if pkgutils.build_package():
                pkgutils.copy_to_cache(pkgbase)
                pkgutils.install_package(var.arguments.noconfirm)
                var.used_pkgbases.add(pkgbase)
                if isRepo: # FIXME: This is executed even with AUR packages
                    if not pkgutils.untrack(pkgbase):
                        io.warn('Could not untrack %s!' % pkgbase)
                if var.arguments.cleanall:
                    pkgutils.cleanup(var.builddir)
                break
            elif not io.prompt('Build failed, try again (%s)?' % pkgbase):
                var.used_pkgbases.add(pkgbase)
                var.failed_packages.add(pkgbase)
                if var.arguments.cleanall:
                    pkgutils.cleanup(var.builddir)
                break
    if len(var.failed_packages) > 0:
        io.fail('Some packages failed to build: %s' % set(var.failed_packages)) # TODO: Improve
except KeyboardInterrupt:
    io.warn('\nCaught Ctrl+C, exiting...', True)
    if var.arguments.cleanall:
        pkgutils.cleanup(var.builddir)
    quit(130) # Ctrl+C exit code
