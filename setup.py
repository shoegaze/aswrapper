#!/usr/bin/env python3

# TODO: Complete it and fix it

from setuptools import setup, find_packages

setup(
    name = 'aswraper',
    version = '0.1',
    packages = find_packages(),
    scripts = [
        'aswrap.py',
        'utils.py'
    ],
    install_requires = [ ]
)
