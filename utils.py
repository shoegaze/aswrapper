#!/usr/bin/env python3

class variables:
    def __init__(self):
        from argparse import ArgumentParser
        from os import getuid, getenv

        self.makepkgflags = str()

        argparser = ArgumentParser() # TODO: '--verbose' flag
        argparser.add_argument('PACKAGES', nargs = '*')
        argparser.add_argument('-c', '--clean', help = 'pass \'--clean\' option to makepkg', action = 'store_true')
        argparser.add_argument('-C', '--cleanall', help = 'clean everything up', action = 'store_true')
        argparser.add_argument('-U', '--updateall', help = 'check for updates for AUR packages', action = 'store_true')
        argparser.add_argument('-u', '--update', help = 'check for updates for AUR packages, but not VCS packages', action = 'store_true')
        argparser.add_argument('-b', '--recursedeps', help = 'Build all the dependencies recursively (broken for now)', action = 'store_true') # TODO
        argparser.add_argument('-Nt', '--notmp', help = 'build in workdir instead of /tmp', action = 'store_true')
        argparser.add_argument('-Nc', '--noconfirm', help = 'do not ask for user input, pass \'--noconfirm\' to makepkg & pacman', action = 'store_true')
        argparser.add_argument('-R', '--norootcheck', help = 'ignore run-as-root checks - DANGEROUS!', action = 'store_true')
        argparser.add_argument('-Df', '--purgecache', help = 'clean package cache', action = 'store_true')
        argparser.add_argument('--skippgpcheck', help = 'pass \'--skippgpcheck\' to makepkg', action = 'store_true')
        # argparser.add_argument('-B', '--sortdeps', help = 'Place dependencies before packages if in PACKAGES', action = 'store_true') # TODO: Should be default
        self.arguments = argparser.parse_args()

        self.packagelist = set(self.arguments.PACKAGES)
        self.used_pkgbases = set()
        self.failed_packages = set()

        self.cachedir = '%s/.cache/aswrapper' % getenv('HOME')

        if getuid() == 0:
            if self.arguments.norootcheck:
                if interactiveness.prompt(interactiveness, 'Are you sure you want to run aswrap as root?'):
                    interactiveness.fail(interactiveness, 'Be extremely careful with what you\'re doing!', True)
                else:
                    quit(1)
            else:
                interactiveness.fail(interactiveness, 'aswrap shouldn\'t be run as root!', True)
                quit(1)

        if self.arguments.cleanall:
            self.arguments.clean = True # As '--cleanall' surely overpowers '--clean'

        if self.arguments.skippgpcheck:
            self.makepkgflags += '-sr --skippgpcheck '
        else:
            self.makepkgflags += '-sr '
        
        if self.arguments.clean:
            self.makepkgflags += '--clean '

        if self.arguments.noconfirm:
            self.makepkgflags += '--noconfirm '

        if self.arguments.notmp:
            from os import getenv
            self.builddir = getenv('PWD')
        else:
            self.builddir = '/tmp/makepkg'
    
    def form_url(self, isRepo, package):
        if isRepo:
            return 'https://www.archlinux.org/packages/search/json/?name=' + package
        return 'https://aur.archlinux.org/rpc/?v=5&type=info&arg=' + package

class colors:
    header = '\033[95m'
    ok_blue = '\033[94m'
    ok_green = '\033[92m'
    warning = '\033[93m'
    fail = '\033[91m'
    nocolor = '\033[0m'
    bold = '\033[1m'
    underline = '\033[4m'

class interactiveness:
    def warn(self, message, bold = False, color = colors.warning):
        if bold:
            print(color + colors.bold + colors.underline + message + colors.nocolor)
        else:
            print(color + colors.underline + message + colors.nocolor)

    def fail(self, message, bold = False, color = colors.fail):
        if bold:
            print(color + colors.bold + message + colors.nocolor)
        else:
            print(color + message + colors.nocolor)

    def prompt(self, query):
        def getch(): # From https://stackoverflow.com/a/36974338
            import tty, sys, termios # raises ImportError if unsupported
            fd = sys.stdin.fileno()
            oldSettings = termios.tcgetattr(fd)
            try:
                tty.setcbreak(fd)
                answer = sys.stdin.read(1)
            finally:
                termios.tcsetattr(fd, termios.TCSADRAIN, oldSettings)
            return answer

        try:
            query += ' [y/N]'
            self.warn(query, True)
            char = getch()
            if char == 'y':
                return True
            elif char == 'n' or '\n' or '\r': # Catch Ctrl-J & Enter as "no"
                return False
            self.fail(self, 'Please answer with a Y/N\n') # FIXME
            return self.prompt(query)
        except KeyboardInterrupt:
            self.warn('\nCaught Ctrl+C, exiting...', True)
            if variables().arguments.cleanall:
                build_functions.cleanup(build_functions, variables().builddir)
            quit(130) # Ctrl+C exit code

class build_functions:
    def create_dir(self, directory):
        from os import makedirs

        makedirs(directory)

    def check_dir(self, directory):
        from os import getenv

        if directory not in [ None, getenv('HOME'), '/', '' ]:
            return True
        return False

    def get_update_packages(self): #TODO: Add checks
        from subprocess import Popen, PIPE
        from shlex import split

        result = list()
        pacman_proc = Popen(split('pacman -Qm'), stdout=PIPE)
        proc_stdout, proc_stderr = pacman_proc.communicate()
        if pacman_proc.returncode == 0:
            raw_pkgs = proc_stdout.decode().split('\n')
            raw_pkgs.pop() # Remove newline
            for data in raw_pkgs:
                noVCS = False
                for vcs in [ 'git', 'svn', 'hg', 'bzr' ]:
                    if data.split(' ')[0].endswith(vcs):
                        result.append([data.split(' ')[0], vcs])
                        break
                    elif not data.split(' ')[0].endswith(vcs) and vcs == "bzr":
                        noVCS = True
                if noVCS:
                    result.append([data.split(' ')[0], data.split(' ')[1].split('-')[0], int(data.split(' ')[1].split('-')[1])])
            return result
        interactiveness.fail(interactiveness, 'Something went wrong while calling pacman!\n%s' % proc_stderr.decode())
        return False
    
    def cleanup(self, directory):
        from shutil import rmtree

        if self.check_dir(directory):
            rmtree(directory)
            return True
        return False
    
    def fetch_json(self, url):
        from urllib.request import urlopen
        from json import loads

        jsfile = urlopen(url).read()
        if jsfile:
            return loads(jsfile)
        return False
    
    def __get_repo_pkgbase(self, package):
        jsfile = self.fetch_json(variables.form_url(variables, True, package))
        if jsfile:
            for block in jsfile['results']:
                if block['pkgname'] == package:
                    return block['pkgbase']
        return False

    def __get_aur_pkgbase(self, package):
        jsfile = self.fetch_json(variables.form_url(variables, False, package))
        if jsfile:
            for block in jsfile['results']:
                if block['Name'] == package:
                    return block['PackageBase']
        return False

    def __get_repo_deps(self, package):
        jsfile = self.fetch_json(variables.form_url(variables, True, package))
        result = set()
        if jsfile:
            for block in jsfile['results']:
                try: # TODO: Use only one 'try' block?
                    for dep in block['depends']:
                        result.add(dep)
                except KeyError:
                    pass
                try:
                    for dep in block['makedepends']:
                        result.add(dep)
                except KeyError:
                    pass
            return result
        return False

    def __get_aur_deps(self, package):
        jsfile = self.fetch_json(variables.form_url(variables, False, package))
        result = set()
        if jsfile:
            for block in jsfile['results']:
                try: # TODO: Use only one 'try' block?
                    for dep in block['Depends']:
                        result.add(dep)
                except KeyError:
                    pass
                try:
                    for dep in block['MakeDepends']:
                        result.add(dep)
                except KeyError:
                    pass
            return result
        return False

    def get_deps(self, package):
        result = self.__get_repo_deps(package)
        if not result:
            result = self.__get_aur_deps(package)
            if not result:
                return False
            return result
        return result

    def get_pkginfo(self, package):
        result = [self.__get_repo_pkgbase(package), True]
        if not result[0]:
            result = [self.__get_aur_pkgbase(package), False]
            if not result[0]:
                return False
            return result
        return result

    def get_aur_pkgversion(self, package):
        jsfile = self.fetch_json(variables.form_url(variables, False, package))
        result = list()
        if jsfile:
            for block in jsfile['results']:
                try:
                    result.append(block['Version'].split('-')[0])
                    result.append(int(block['Version'].split('-')[1]))
                except KeyError:
                    pass
            return result
        return False

    def fetch_package(self, isRepo, package): # TODO: Don't use asp
        from shlex import quote, split
        from subprocess import call, DEVNULL
        from git import Repo

        if isRepo:
            interactiveness.warn(interactiveness, 'Exporting %s from repositories using ASP...' % package, True)
            if call(split('asp export %s' % quote(package)), stdout = DEVNULL) == 0:
                return True
            interactiveness.fail(interactiveness, 'Couldn\'t export %s:\n%s' % package, True)
            return False
        else:
            interactiveness.warn(interactiveness, 'Fetching %s from AUR...' % package, True)
            if Repo.clone_from('https://aur.archlinux.org/%s.git' % package, '%s/%s' % (variables().builddir, package)):
                return True
            interactiveness.fail(interactiveness, 'Couldn\'t fetch %s from AUR!' % package, True)
            return False

    def edit_package(self):
        from os import getenv
        from shlex import quote, split
        from subprocess import call

        if call(split('%s PKGBUILD' % quote(getenv('EDITOR')))) == 0: # TODO: Test
            return True
        interactiveness.fail(interactiveness, 'Something went wrong with the editor!')
        return False
    
    def build_package(self):
        from shlex import split
        from subprocess import call

        if call(split('env BUILDDIR=%s makepkg %s' % (variables().builddir, variables().makepkgflags))) == 0:
            return True
        return False

    def install_package(self, noconfirm = False):
        from shlex import split
        from subprocess import call
        from glob import glob

        pkgfiles = ' '.join(glob('*.pkg.*'))
        if noconfirm:
            if call(split('sudo pacman -U --noconfirm %s' % pkgfiles)) == 0:
                return True
            return False
        else:
            if call(split('sudo pacman -U %s' % pkgfiles)) == 0:
                return True
            return False
    
    def untrack(self, package):
        from shlex import quote, split
        from subprocess import call

        if call(split('asp untrack %s' % quote(package))):
            return True
        return False
    
    def copy_to_cache(self, pkgbase):
        from os import path, makedirs, remove
        from glob import glob
        from shutil import copy

        if not path.exists(variables().cachedir):
            makedirs(variables().cachedir)

        pkgfiles = glob('*.pkg.*')
        for file in pkgfiles:
            full_path = path.join(variables().builddir, pkgbase, file)
            if path.exists(path.join(variables().cachedir, file)):
                remove(path.join(variables().cachedir, file))
            copy(full_path, variables().cachedir)
